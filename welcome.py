from discord import Forbidden
from discord.ext.commands import Cog
from discord.ext.commands import command

from ..db import db

class Welcome(Cog):
    def __init__(self,bot):
        self.bot = bot
    
    @Cog.listener()
    async def on_ready(self):
        if not self.bot.ready:
            self.bot.cogs_ready.ready_up("welcome")

    @Cog.listener()
    async def on_member_join(self, member):
        db.execute("INSERT INTO exp (UserID) VALUES (?)", membed.id)
        await self.bot.get_channel(926337228060364810).send(f"Welcome to **{member.guild.name}** {member.mention}! Head over to <#913821725605630073> to say hi")

        #below cmd for sending direct message to new members
        try:
            await member.send(f"Welcome to **{member.guild.name}**! Enjoy your stay!")  
        except Forbidden:
            pass

        await member.add_roles(member.guild.get_role(914034262586589214), member.guild.get_role(918212944033095800))   #list of roles can be done too

        #faster method for giving roles is this (preferable for )
        #await member.edit(roles=[*member.roles, *[member.guild.get_role(id_) for id_ in (914034262586589214, 918212944033095800)]])


    @Cog.listener()
    async def on_member_leave(self, member):
        db.execute("DELETE FROM exp WHERE UserID = ?", member.id)
        await self.bot.get_channel(926337228060364810).send(f"{member.display_name} has left {member.guild.name}.")


def setup(bot):
    bot.add_cog(Welcome(bot))